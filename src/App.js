import React, { useEffect, useState } from 'react';
import logo from './images/Logo.png';
import './App.css';
import './libraries/materialize/ghpages-materialize.css';
import './libraries/materialize/transitions.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
  // useRouteMatch,
  // useParams
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import PokemonDetail from './components/PokemonDetail/PokemonDetail.js';
import PokemonsCatalog from './components/PokemonsCatalog/PokemonsCatalog.js';

function App() {
  var location = window.location.href;
  var locationAux = 'es';
  if(location.indexOf('language') >= 0){
    locationAux = location.substr(location.indexOf('language/') + 9, location.length);
    if(locationAux.indexOf('/') >= 0){
      locationAux = location.substr(0, location.indexOf('/'));
    }
  }
  var [language, setLanguage] = useState(locationAux);
  

  useEffect(() => {
      setTimeout(() => {
        document.getElementsByTagName('nav')[0].className = 'fade-in-from-bottom';
        setTimeout(() => {
          document.getElementsByClassName('content-body')[0].className = 'content-body fade-in';
        }, 1000);
      }, 1300);
  });

  var changeLanguage = (evt) => {
    setLanguage(evt.currentTarget.value);
    var location = window.location.href;
    if(location.indexOf('language') >= 0){
      window.location.href = location.substr(0, location.indexOf('language/') + 9) + evt.currentTarget.value;
    }else{
      if(location[location.length - 1] == '/'){
        window.location.href = location + 'language/' + evt.currentTarget.value;
      }else{
        window.location.href = location + '/language/' + evt.currentTarget.value;
      }
    }
  }

  return (
    <Router>
      <div style={{
			height: "64px",
			overflow: "hidden",
      }}>
        <nav className="main hidden">
          <div className="nav-wrapper">
            <a href="/" className="brand-logo"><img style={{
              marginLeft: "10px",
              height: "50px"
            }} alt="logo" src={logo}></img></a>
            {(()=>{
                if(language === 'en'){
                  return <ul className="tabs right hide-on-med-and-down">
                    <li className="tab col s3" style={{
                      width: "auto"
                    }}><Link target="" to="/language/en">Pokemons</Link></li>
                    <li className="tab col s3" style={{
                      width: "auto"
                    }}><Link to="/about/language/en">About</Link></li>
                    <li className="tab col s3" style={{
                      width: "auto"
                    }}><select style={{
                      display: "inline-block",
                      width: "max-content",
                      marginRight: "10px",
                      background: "none",
                      color: "white",
                      border: "none"
                    }} value={language} onChange={changeLanguage.bind(this)}>
                      <option value="es" style={{
                        color: "black",
                      }}>ESPAÑOL</option>
                      <option value="en" style={{
                        color: "black",
                      }}>ENGLISH</option>
                    </select></li>
                  </ul>
                }else{
                  return <ul className="tabs right hide-on-med-and-down">
                    <li className="tab col s3" style={{
                      width: "auto"
                    }}><Link target="" to="/language/es">Pokemons</Link></li>
                    <li className="tab col s3" style={{
                      width: "auto"
                    }}><Link to="/about/language/es">Acerca de</Link></li>
                    <li className="tab col s3" style={{
                      width: "auto"
                    }}><select style={{
                      display: "inline-block",
                      width: "max-content",
                      marginRight: "10px",
                      background: "none",
                      color: "white",
                      border: "none"
                    }} value={language} onChange={changeLanguage.bind(this)}>
                      <option value="es" style={{
                        color: "black",
                      }}>ESPAÑOL</option>
                      <option value="en" style={{
                        color: "black",
                      }}>ENGLISH</option>
                    </select></li>
                  </ul>
                }
            })()}
          </div>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      </div>
      <div className="content-body hidden">
        <Switch>
          <Route path="/about/language/:language" component={About}></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/pokemon/:id/language/:language" component={PokemonDetail}></Route>
          <Route path="/pokemon/:id/" component={PokemonDetail}></Route>
          <Route path="/language/:language" component={PokemonsCatalog}></Route>
          <Route path="/" component={PokemonsCatalog}></Route>
        </Switch>
      </div>
    </Router>
  );
}


function About() {
  return <h2>Bienvenido! Ojala les haya gustado mi presentación! 
    <br></br>
    <br></br>
    Las transiciones muchas estan hechas a mano y otras utilizan parte de la libreria materialize, pero customizada para mi gusto
    <br></br>
    <br></br>
    La página es totalmente single-page excepto para cambiar de idioma, lo cual podria mejorar en rendimiento y podria ser single page si se utiliza el storage de redux para guardar variables, pero no sabia si estaba permitido usarlo.
    <br></br>
    <br></br>
    Muchas gracias
    <br></br>
    Pablo Iglesias
    </h2>;
}


export default App;
