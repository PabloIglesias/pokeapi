import { useEffect } from "react";
import React from 'react';
import $ from "jquery";
import './column-drop.css';

var ColumnDropComponent = (props) => {
	var columnOne = props.columnOne;
	var columnTwo = props.columnTwo;
	var columnThree = props.columnThree;

	useEffect(() => {
		var i = 0;
		var order = [1, 0, 2];
		var elems = $('.scale-transition');
		var interval = setInterval(() => {
			if(i < order.length){
				//$('.scale-transition')[i].classList.remove('scale-out');
				elems[order[i]].className = 'scale-transition scale-in';
			}else{
				clearInterval(interval);
			}
			i++;
		}, 150);
	});

	return (
		<div className=".md-content column-drop-container">
			<div className="container">
				<section className="layout columna2">
					<div className="scale-transition scale-out">
						{columnOne}
					</div>
				</section>
				<section className="layout columna1">
					<div className="scale-transition scale-out">
						{columnTwo}
					</div>
				</section>
				<section className="layout columna3">
					<div className="scale-transition scale-out">
						{columnThree}
					</div>
				</section>
			</div>
		</div>
	);
}


export default ColumnDropComponent;