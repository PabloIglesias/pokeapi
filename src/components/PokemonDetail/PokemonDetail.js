import React, { useState, useEffect } from 'react';
import './PokemonDetail.css';

export default function PokemonDetail(props) {
    var pokemonId = props.match.params.id;
    var language = props.match.params.language;
    var [actualImage, setActualImage] = useState('');
    var [imageList, setImageList] = useState([]);
    var pokemonData = [];
    var pokemonDescription = [];
    var pokemonAbilitiesApi = [];
    var [pokemonName, setPokemonName] = useState('');
    var [flavorTextEntry, setFlavorTextEntry] = useState('');
    var [pokemonTypes, setTypes] = useState([]);
    var [pokemonAbilities, setAbilities] = useState([]);
    var htmlBars = [];
    for(var i = 0; i < 120; i++){
        htmlBars.push(<div class="stat-bar grey"></div>);
    }
    var getPokemonData = async () => {
        let response = await fetch('https://pokeapi.co/api/v2/pokemon/'+pokemonId);
        pokemonData = await response.json();
        response = await fetch('https://pokeapi.co/api/v2/pokemon-species/'+pokemonId);
        pokemonDescription = await response.json();
        for(var i = 0; i < pokemonData.abilities.length; i++){
            let abilityObject = pokemonData.abilities[i];
            let response = await fetch(abilityObject.ability.url);
            let json = await response.json();
            let flavorTextEntry = json.flavor_text_entries.filter((flavor_text_entry) => {
                if(language){
                    if(flavor_text_entry.language.name === language){
                        return flavor_text_entry;
                    }
                    return null;
                }else{
                    if(flavor_text_entry.language.name === 'en'){
                        return flavor_text_entry;
                    }
                    return null;
                }
            })[0];
            pokemonAbilitiesApi.push({
                name: json.name,
                description: flavorTextEntry.flavor_text
            });
        }
        setPokemonName(pokemonDescription.name);
        setFlavorTextEntry(pokemonDescription.flavor_text_entries.filter((flavor_text_entry) => {
            if(language){
                if(flavor_text_entry.language.name === language){
                    return flavor_text_entry;
                }
                return false;
            }else{
                if(flavor_text_entry.language.name === 'en'){
                    return flavor_text_entry;
                }
                return false;
            }
        })[0]);
        setTypes(pokemonData.types);
        setAbilities(pokemonAbilitiesApi);
        setActualImage(pokemonData.sprites.front_default);
        let aux = Object.values(pokemonData.sprites);
        setImageList(aux.filter((sprite) => {
            if(typeof(sprite) === "string" && sprite !== undefined)
                return sprite;
            return null;
        }));
        if(document.getElementsByClassName('PokemonDetail')[0]){
            document.getElementsByClassName('PokemonDetail')[0].className = 'PokemonDetail fade-in';
        }
        setTimeout(() => {
            for(i = 0; i < pokemonData.stats.length; i++){
                var statData = pokemonData.stats[i];
                    if(document.getElementsByClassName("stat-bars "+statData.stat.name+" col-9")[0]){
                    var statBars = document.getElementsByClassName("stat-bars "+statData.stat.name+" col-9")[0].childNodes;
                    document.getElementsByClassName("stat-bars "+statData.stat.name+" col-9")[0].getElementsByClassName('stat-value')[0].innerHTML = statData.base_stat;
                    for(var j = 0; j < statData.base_stat; j++){
                        statBars[j].className = "stat-bar grey scale-transition scale-out";
                        timerHandler(statBars[j]);
                    }
                }
            }
        }, 2500);
        return;
    }

    var timerHandler = (elem) => {
        setTimeout((pointer)=>{
            if(elem){
                elem.className = "stat-bar green scale-transition scale-in";
            }
        }, 300);
    }
    
    useEffect(() => {
        getPokemonData();
    }, []);

    var changeAbility = (ability, event) => {
        if(document.getElementsByClassName('ability-description')[0]){
            var descElem = document.getElementsByClassName('ability-description')[0];
            var target = event.currentTarget;
            target.disabled = true;
            if(descElem.className === "ability-description scale-transition scale-in row"){
                descElem.className = "ability-description scale-transition scale-out row";
                setTimeout(()=>{
                    if(descElem && descElem.childNodes[0]){
                        descElem.childNodes[0].innerHTML = ability.description;
                        descElem.className = "ability-description scale-transition scale-in row";
                        target.disabled = false;
                    }
                }, 300)
            }else{
                descElem.childNodes[0].innerHTML = ability.description;
                descElem.className = "ability-description scale-transition scale-in row";
                target.disabled = false;
            }
        }
    }

    var changeImage = (image, event) => {
        setActualImage(image);
    }

    return <div className="PokemonDetail hidden">
        <div className="row">
            <div className="PokemonViewer fade-in col col-6">
                <div className="image-container">
                    <img alt="" src={actualImage}></img>
                </div>
                <div className="images-list-container">
                    <div style={{
                        width: "max-content"
                    }}>
                    {
                        imageList.map((image)=>{
                            return <img alt="" key={image} src={image} onClick={changeImage.bind(this, image)}></img>
                        })
                    }
                    </div>
                </div>
            </div>
            <div className="PokemonDetail fade-in col col-6">
                <div className="pokemon-name">
                    <h3>{pokemonName}</h3>

                </div>
                <div className="pokemon-description row">
                    <h5>{flavorTextEntry ? flavorTextEntry.flavor_text : ''}</h5>
                </div>
                <div className="pokemon-type row">{pokemonTypes ? pokemonTypes.map(typeObject => typeObject.type.name).join(', ') : ''}</div>
                <div className="pokemon-habilities row">
                    <div className="ability-container row">
                        {pokemonAbilities.map((ability) => {
                                return<div key={ability.name} className="ability-name inline"><button onClick={changeAbility.bind(this, ability)}>{ability.name}</button></div>
                        })}
                        <div className={"ability-description scale-transition scale-out row"}><div></div></div>
                    </div>
                </div>
                
            </div>

        </div>
        <div className="PokemonStats fade-in row">
            {(() => {
                if(language === 'es'){
                    return <div class="row">
                        <div className="stat-value row"><h4 className="col-3">PV: </h4><div className="stat-bars hp col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">ATAQUE: </h4><div className="stat-bars attack col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">DEFENSA: </h4><div className="stat-bars defense col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">ATAQUE ESPECIAL: </h4><div className="stat-bars special-attack col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">DEFENSA ESPECIAL: </h4><div className="stat-bars special-defense col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">VELOCIDAD: </h4><div className="stat-bars speed col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                    </div>
                }else{
                    return <div class="row">
                        <div className="stat-value row"><h4 className="col-3">HP: </h4><div className="stat-bars hp col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">ATTACK: </h4><div className="stat-bars attack col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">DEFENSE: </h4><div className="stat-bars defense col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">SPECIAL ATTACK: </h4><div className="stat-bars special-attack col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">SPECIAL DEFENSE: </h4><div className="stat-bars special-defense col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                        <div className="stat-value row"><h4 className="col-3">SPEED: </h4><div className="stat-bars speed col-9">{htmlBars}(<div class="stat-value">{0}</div>)</div></div>
                    </div>
                }
            })()}
        </div>
    </div>;
}