import React, { useEffect, useState } from 'react';
import './PokemonsCatalog.css';
import FirstColumnImage from '../../images/pokemon.png';
import ThirdColumnImage from '../../images/Poke_Ball.png';
import ColumnDropComponent from '../column-drop/column-drop.js';
import {
    Link
} from "react-router-dom";
import { debugContextDevtool } from 'react-context-devtool';

var PokemonsCatalog = (props) => {
    var column1 = <img className="first-column img" src={FirstColumnImage} alt=""></img>;
    var language = props.match.params.language ?? 'es';
    var limitList = 5;
    // var initialPage = 0;
    var actualPage = 0;
    var pages = 0;
    var pokemonsApi;
    var pokemonList = [];
    var [column2, setStateColumn2] = useState('');
    var column3 = <img className="third-column img" src={ThirdColumnImage} alt=""></img>;
    
    useEffect(() => {
        fetch("https://pokeapi.co/api/v2/pokemon")
            .then(res => res.json())
            .then(
            (result) => {
                pokemonsApi = result;
                pages = pokemonsApi.results.length/limitList;
                pokemonsLoader();
            },
            // Nota: es importante manejar errores aquí y no en 
            // un bloque catch() para que no interceptemos errores
            // de errores reales en los componentes.
            (error) => {
                setStateColumn2(
                    <h2>{JSON.stringify(error)}</h2>
                );
            }
        );
    }, []);

    var changePage = (page, event) =>{
        actualPage = page;
        setStateColumn2("");
        pokemonsLoader();
    }

    var pokemonsLoader = async () => {
        var pokemon;
        pokemonList = [];
        for(var i = actualPage*limitList; i <  (actualPage*limitList)+limitList; i++){
            pokemon = pokemonsApi.results[i];
            await addPokemonToList(pokemon, pokemonList);
        }
        setStateColumn2(
            <div className="Column2">
                <ul className="pokemons-list row">
                    {
                        pokemonList.map((pokemon) => {
                            return <li key={pokemon.id} className="col col-4">
                                <div className="pokemon-container" name={pokemon.name}>
                                    <Link target="" to={"/pokemon/" + pokemon.id + '/language/' + language}>
                                        <img className="pokemon-image" src={pokemon.sprites.front_default} alt=""></img>
                                        <h3 className="pokemon-name">{pokemon.name}</h3>
                                    </Link>
                                </div>
                            </li>;
                        })
                    }
                </ul>
                <div className="paginator">
                    {(()=>{
                        var html = [];
                        if(actualPage > 0){
                            html.push(<button key={"prev"} onClick={changePage.bind(this, actualPage - 1)}>{language === 'en' ? 'PREV' : 'ANTERIOR'}</button>);
                        }
                        for(var i = 1; i <= pages; i++){
                            if(actualPage === (i - 1)){
                                html.push(<button className="selected-page" key={i -  1}>{i}</button>);
                            }else{
                                html.push(<button key={i -  1} onClick={changePage.bind(this, i - 1)}>{i}</button>);
                            }
                        }
                        if(actualPage < pages - 1){
                            html.push(<button key={"next"} onClick={changePage.bind(this, actualPage + 1)}>{language === 'en' ? 'NEXT' : 'PROXIMA'}</button>);
                        }
                        return html;
                    })()}
                </div>
           </div>
        );
    };

    var addPokemonToList = async (pokemon, pokemonList) => {
        const response = await fetch(pokemon.url);
        const json = await response.json();
        pokemonList.push(json);
           
    }

    return <div className="PokemonsCatalog">
        <ColumnDropComponent columnOne={column1} columnTwo={column2} columnThree={column3}></ColumnDropComponent>
    </div>;
}

export default PokemonsCatalog;
  